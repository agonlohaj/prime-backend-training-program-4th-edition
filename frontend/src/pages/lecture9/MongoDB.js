/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import { Bold } from "presentations/Label";
import Code from "presentations/Code";
import SimpleLink from "presentations/rows/SimpleLink";
import DocumentImage from "assets/images/lecture9/document.png";
import PageLink from "../../presentations/rows/nav/PageLink";

const styles = ({ size, typography }) => ({
  root: {
  },
  section: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    marginTop: size.spacing / 2
  },
  piece: {
    flex: 1,
    display: 'flex',
    alignItems: 'flex-start',
    flexFlow: 'row wrap',
    marginRight: size.spacing * 2,
    '& :last-child': {
      marginRight: 0
    }
  }
})

const jsonExample = `{
  "_id": "5cf0029caff5056591b0ce7d",
  "firstname": "Jane",
  "lastname": "Wu",
  "address": {
    "street": "1 Circle Rd",
    "city": "Los Angeles",
    "state": "CA",
    "zip": "90404"
  },
  "hobbies": ["surfing", "coding"]
}`

const richQueries = `> db.users.find({ "address.zip" : "90404" })
{ "_id": "5cf0029caff5056591b0ce7d", "firstname": "Jane", "lastname": "Wu", "address": { "zip": "90404" } }
{ "_id": "507f1f77bcf86cd799439011", "firstname": "Jon", "lastname": "Davis", "address": { "zip": "90404" } }
{ "_id": "5349b4ddd2781d08c09890f3", "firstname": "Jim", "lastname": "White", "address": { "zip": "90404" } }
{ "_id": "5bf142459b72e12b2b1b2cd", "firstname": "Jeff", "lastname": "Taylor", "address": { "zip": "90404" } }
{ "_id": "5cf003283b23d04a40d5f88a", "firstname": "Jerry", "lastname": "Miller", "address": { "zip": "90404" } }
{ "_id": "5bf142459b72e12b2b1b2cd", "firstname": "Jai", "lastname": "Williams", "address": { "zip": "90404" } }
{ "_id": "5cf0036deaa1742dd225ea35", "firstname": "Jess", "lastname": "Johnson", "address": { "zip": "90404" } }
{ "_id": "54495ad94c934721ede76d90", "firstname": "Jill", "lastname": "Brown", "address": { "zip": "90404" } }
{ "_id": "566eb3c704c7b31facbb0007", "firstname": "Janet", "lastname": "Jones", "address": { "zip": "90404" } }
{ "_id": "5a999cc461d36489a27f2563", "firstname": "Jan", "lastname": "Smith", "address": { "zip": "90404" } }`

const transactionExample = `session.start_transaction()
order = { line_items : [ { item : 5, quantity: 6 } ] }
db.orders.insertOne( order, session=session );
for x in order.line_items:
  db.inventory.update(
    { _id  : x.item } ,
    { $inc : { number : -1 * x.quantity } },
    session=session
  )
session.commit_transaction()`

const exampleRoutes = `DELETE         /api/raw/taxi/:id           @io.training.api.controllers.TaxiRawMongoController.delete(request: Request, id: String)
PUT            /api/raw/taxi/:id           @io.training.api.controllers.TaxiRawMongoController.update(request: Request, id: String)
POST           /api/raw/taxi               @io.training.api.controllers.TaxiRawMongoController.save(request: Request)
GET            /api/raw/taxi               @io.training.api.controllers.TaxiRawMongoController.all(request: Request)`

const mongoDBKickStart = `MongoClient mongo = MongoClients.create();
MongoDatabase database = mongo.getDatabase("training");
MongoCollection<Document> collection = database.getCollection("raw");`

const MongoDB = (props) => {
  const { classes, section } = props
  const examples = section.children[0]
  // const specification = section.children[1]
  // const membership = section.children[2]
  // const serialization = section.children[3]
  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 9: {section.display}
        <Typography>
          The database for modern applications
        </Typography>
        <Divider />
      </Typography>
      <Typography>
        MongoDB is a general purpose, document-based, distributed database built for modern application developers and for the cloud era.
      </Typography>
      <Typography>
        MongoDB is a document database, which means it stores data in JSON-like documents. We believe this is the most natural way to think about data, and is much more expressive and powerful than the traditional row/column model.
      </Typography>

      <div className={classes.section}>
        <Typography className={classes.piece}>
          <Bold>Rich JSON Documents.</Bold><br/>
          <ul>
            <li>The most natural and productive way to work with data.</li>
            <li>Supports arrays and nested objects as values.</li>
            <li>Allows for flexible and dynamic schemas.</li>
          </ul>
        </Typography>
        <div className={classes.piece}>
          <Code>
            {jsonExample}
          </Code>
        </div>
      </div>
      <div className={classes.section}>
        <div className={classes.piece}>
          <Code>
            {richQueries}
          </Code>
        </div>
        <Typography className={classes.piece}>
          <Bold>Powerful query language.</Bold><br/>
          <ul>
            <li>Rich and expressive query language that allows you to filter and sort by any field, no matter how nested it may be within a document.</li>
            <li>Support for aggregations and other modern use-cases such as geo-based search, graph search, and text search.</li>
            <li>Queries are themselves JSON, and thus easily composable. No more concatenating strings to dynamically generate SQL queries.</li>
          </ul>
        </Typography>
      </div>
      <div className={classes.section}>
        <Typography className={classes.piece}>
          <Bold>All the power of a relational database, and more...</Bold><br/>
          <ul>
            <li>Distributed multi-document ACID transactions with snapshot isolation.</li>
            <li>Support for joins in queries.</li>
            <li>Two types of relationships instead of one: reference and embedded.</li>
          </ul>
        </Typography>
        <div className={classes.piece}>
          <Code>
            {transactionExample}
          </Code>
        </div>
      </div>

      <Typography id={examples.id} variant={'title'}>
        {examples.display}
      </Typography>

      <Typography>
        MongoDB’s document model is simple for developers to learn and use, while still providing all the capabilities needed to meet the most complex requirements at any scale. Mongo provide drivers for 10+ languages, and the community has built dozens more.
      </Typography>
      <div className={classes.section}>
        <Typography className={classes.piece}>
          <ul>
            <li>MongoDB <Bold>stores data in flexible, JSON-like documents</Bold>, meaning fields can vary from document to document and data structure can be changed over time</li>
            <li>The document model <Bold>maps to the objects in your application code</Bold>, making data easy to work with</li>
            <li><Bold>Ad hoc queries, indexing, and real time aggregation</Bold> provide powerful ways to access and analyze your data</li>
            <li>MongoDB is a <Bold>distributed database at its core</Bold>, so high availability, horizontal scaling, and geographic distribution are built in and easy to use</li>
            <li>MongoDB is <Bold>free to use</Bold>. Versions released prior to October 16, 2018 are published under the AGPL. All versions released after October 16, 2018, including patch fixes for prior versions, are published under the <SimpleLink href="https://www.mongodb.com/licensing/server-side-public-license">Server Side Public License (SSPL) v1.</SimpleLink></li>
          </ul>
        </Typography>
        <div className={classes.piece}>
          <img style={{ width: 480 }} src={DocumentImage}/>
        </div>
      </div>
      <Typography>
        Let us investigate and work a bit with Mongo to see how it works. The simplest start would be to use:
        <Code>
          {mongoDBKickStart}
        </Code>
        And our routes:
        <Code>
          {exampleRoutes}
        </Code>
        Here are more examples on these exercises: <SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/driver/tutorials/databases-collections/">Mongo Databases and Collections</SimpleLink>
      </Typography>
      <Typography fontStyle="italic">
        <ul>
          <li>If you haven't installed Mongo follow the <Bold><PageLink to="/section/mongodb/">Getting Started</PageLink></Bold> section at the Setup and Introduction.</li>
          <li>Too run Mongo DB, follow the <Bold><PageLink to="/section/runningtheapplication/">Running the Application</PageLink></Bold> guide at Project Setup".</li>
        </ul>
      </Typography>
      <Typography>
        To learn more about the topics covered in this page, follow the links below:
        <ul>
          <li><SimpleLink href="https://www.mongodb.com/">https://www.mongodb.com/</SimpleLink></li>
          <li><SimpleLink href="https://www.mongodb.com/what-is-mongodb">https://www.mongodb.com/what-is-mongodb</SimpleLink></li>
          <li><SimpleLink href="https://docs.mongodb.com/manual/">https://docs.mongodb.com/manual/</SimpleLink></li>
          <li><SimpleLink href="https://university.mongodb.com/">https://university.mongodb.com/</SimpleLink></li>
          <li><SimpleLink href="https://docs.mongodb.com/manual/introduction/">https://docs.mongodb.com/manual/introduction/</SimpleLink></li>
          <li><SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/driver/getting-started/installation/">https://mongodb.github.io/mongo-java-driver/4.1/driver/getting-started/installation/</SimpleLink></li>
          <li><SimpleLink href="https://github.com/mongodb/mongo-java-driver/tree/master">https://github.com/mongodb/mongo-java-driver/tree/master</SimpleLink></li>
        </ul>
      </Typography>

    </Fragment>
  )
}

export default withStyles(styles)(MongoDB)
