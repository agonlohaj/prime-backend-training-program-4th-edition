/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";

const styles = ({ size, palette, typography }) => ({
  root: {}
})
const Assignments = (props) => {
  const { classes, section } = props
  // const pagination = section.children[0]
  // const aggregations = section.children[1]
  // const algorithm = section.children[2]
  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 11: {section.display}
        <Divider />
      </Typography>

      <Typography>
        Title: "Implement test cases for the previous assignments"<br/>
        Description: "For each of the routes used to handle the assignments, a test cases will exists that verifies the solution"
      </Typography>
    </Fragment>
  )
}

export default withStyles(styles)(Assignments)
