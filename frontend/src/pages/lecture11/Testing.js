/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import { connect } from 'react-redux';
import SimpleLink from "../../presentations/rows/SimpleLink";
import Code from "../../presentations/Code";
import { Highlighted } from "../../presentations/Label";

const styles = ({ typography }) => ({
  root: {},
})

const jUnitExample = `public class SimpleTest {

	@Test
	public void testSum() {
		int a = 1 + 1;
		assertEquals(2, a);
	}

	@Test
	public void testString() {
		String str = "Hello world";
		assertFalse(str.isEmpty());
	}
}`

const javaOptions = `javaOptions in Test ++= Seq(
  "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=9998",
  "-Xms512M",
  "-Xmx1536M",
  "-Xss1M",
  "-XX:MaxPermSize=384M"
)`

const testingControllers = `public class ApplicationTestCases extends WithApplication {

	@Test
	public void testIndex() {
		final Http.RequestBuilder homeRequest = new Http.RequestBuilder().method("GET").uri("/");
		final Result result = route(app, homeRequest);
		assertEquals(OK, result.status());
		assertEquals("text/html", result.contentType().get());
		assertEquals("utf-8", result.charset().get());
		assertTrue(contentAsString(result).contains("Welcome to your Play Web Application!"));
	}
}`

const testingResponses = `@Test
public void testMongo() {
  final Http.RequestBuilder homeRequest = new Http.RequestBuilder().method("GET").uri("/mongo");
  final Result result = route(app, homeRequest);
  assertEquals("application/json", result.contentType().get());
  assertEquals(OK, result.status());

  JsonNode body = Json.parse(contentAsString(result));
  List<Taxi> taxis = DatabaseUtils.parseJsonListOfType(body, Taxi.class);
  assertEquals("Expected the collection to have no elements!", taxis.size(), 0);
}`

class Testing extends React.Component {

  render() {
    const { classes, section } = this.props
    const overview = section.children[0]
    const jUnit = section.children[1]
    const controllers = section.children[2]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Typography variant='p'>
            In this section we are going to introduce the concept of Testing our Routes and Implementations!
          </Typography>
          <Divider />
        </Typography>
        <Typography id={overview.id} variant={'title'}>
          {overview.display}
        </Typography>
        <Typography>
          Writing tests for your application can be an involved process. Play supports JUnit and provides helpers and application stubs to make testing your application as easy as possible.
        </Typography>
        <Typography>
          The location for tests is in the “test” folder. There are two sample test files created in the test folder which can be used as templates.
        </Typography>
        <Typography>
          You can run tests from the sbt console.
          <ul>
            <li>To run all tests, run test.</li>
            <li>To run only one test class, run testOnly followed by the name of the class i.e. testOnly my.namespace.MyTest.</li>
            <li>To run only the tests that have failed, run testQuick.</li>
            <li>To run tests continually, run a command with a tilde in front, i.e. ~testQuick.</li>
            <li>To access test helpers such as FakeApplication in console, run test:console.</li>
          </ul>
          Testing in Play is based on <SimpleLink href="https://www.scala-sbt.org/">sbt</SimpleLink>, and a full description is available in the <SimpleLink href="https://www.scala-sbt.org/release/docs/Testing.html">testing documentation</SimpleLink>.
        </Typography>

        <Typography id={jUnit.id} variant={'title'}>
          {jUnit.display}
        </Typography>
        <Typography>
          The default way to test a Play application is with JUnit.
          <Code>{jUnitExample}</Code>
        </Typography>
        <Typography>
          <Highlighted>
            Note: A new process is forked each time test or test-only is run. The new process uses default JVM settings. Custom settings can be added to build.sbt. For example:
          </Highlighted>
          <Code>
            {javaOptions}
          </Code>
          In our case:
          <Code>
            javaOptions in Test ++= Seq("-Dconfig.file=conf/test.conf", "-Dlogger.resource=test.logback.xml")
          </Code>
        </Typography>

        <Typography id={controllers.id} variant={'title'}>
          {controllers.display}
        </Typography>
        <Typography>
          You can test your controllers using Play’s test helpers to extract useful properties.
          <Code>
            {testingControllers}
          </Code>
          Or when it comes to getting JSON responses back:
          <Code>
            {testingResponses}
          </Code>
        </Typography>
      </Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = (dispatch) => ({
})

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Testing))
