/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import { Bold, Highlighted } from "presentations/Label";
import Code from "presentations/Code";
import SimpleLink from "presentations/rows/SimpleLink";
import WebSocket from "presentations/WebSocket";

const styles = ({ size, typography }) => ({
  root: {
  }
})

const collectionSimple = `MongoCollection<Taxi> collection = mongoDB.getMongoDatabase().getCollection("pojo", Taxi.class);
List<Bson> pipeline = new ArrayList<>();

pipeline.add(Aggregates.match(
  Filters.and(
    Filters.in("operationType", Collections.singletonList("insert")),
    Filters.eq("fullDocument.name", "Alice")
  )
)); // we are interested only on new documents that are inserted with the name "Alice"

ChangeStreamIterable<Taxi> changeStreamDocuments = collection.watch(pipeline, Taxi.class); // given the conditions above, start watching`

const ChangeStreams = (props) => {
  const { classes, section } = props
  // const examples = section.children[0]
  // const specification = section.children[1]
  // const membership = section.children[2]
  // const serialization = section.children[3]
  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 10: {section.display}
        <Typography>
          <SimpleLink href="https://docs.mongodb.com/manual/changeStreams/#resume-a-change-stream">Change Streams</SimpleLink> (Since version 3.6)
        </Typography>
        <Divider />
      </Typography>

      <Typography>
        Change streams allow applications to access real-time data changes without the complexity and risk of tailing the oplog. Applications can use change streams to subscribe to all data changes on a single collection, a database, or an entire deployment, and immediately react to them. Because change streams use the aggregation framework, applications can also filter for specific changes or transform the notifications at will.
      </Typography>

      <Typography>
        Change streams are available for replica sets and sharded clusters:
        <ol>
          <li>
            <Bold>Storage Engine.</Bold><br/>
            The replica sets and sharded clusters must use the WiredTiger storage engine. Change streams can also be used on deployments that employ MongoDB’s encryption-at-rest feature.
          </li>
          <li>
            <Bold>Replica Set Protocol Version.</Bold><br/>
            The replica sets and sharded clusters must use replica set protocol version 1 (pv1).
          </li>
          <li>
            <Bold>Read Concern “majority” Enablement.</Bold><br/>
            Starting in MongoDB 4.2, change streams are available regardless of the "majority" read concern support; that is, read concern majority support can be either enabled (default) or disabled to use change streams.
            In MongoDB 4.0 and earlier, change streams are available only if "majority" read concern support is enabled (default).
          </li>
        </ol>
        You can open change streams against:
        <ol>
          <li>
            <Bold>A collection</Bold><br/>
            You can open a change stream cursor for a single collection (except system collections, or any collections in the admin, local, and config databases).
          </li>
          <li>
            <Bold>A database</Bold><br/>
            Starting in MongoDB 4.0, you can open a change stream cursor for a single database (excluding admin, local, and config database) to watch for changes to all its non-system collections.
          </li>
          <li>
            <Bold>A deployment</Bold><br/>
            Starting in MongoDB 4.0, you can open a change stream cursor for a deployment (either a replica set or a sharded cluster) to watch for changes to all non-system collections across all databases except for admin, local, and config.
          </li>
        </ol>
        Let us look at the following example which exists at our <Highlighted>MongoMonitoringActor</Highlighted>
        <Code>
          {collectionSimple}
        </Code>
        Once we add a new document at the route: <Highlighted>http://localhost:9000/api/pojo/taxi</Highlighted>, which inserts a new item at the "pojo" collection of type Taxi, the watcher below will receive the document inserted given the subscription above:
      </Typography>

      <WebSocket
        endpoint={'/lecture10/watcher'}
        readOnly={true}
      />
    </Fragment>
  )
}

export default withStyles(styles)(ChangeStreams)
