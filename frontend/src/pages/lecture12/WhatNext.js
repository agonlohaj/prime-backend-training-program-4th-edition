/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "../../presentations/rows/SimpleLink";

const styles = ({ typography }) => ({
  root: {},
})

class WhatNext extends React.Component {
  render() {
    const { classes, section } = this.props
    return (
      <Fragment>
        <Typography variant={'heading'}>
          What was not Included?
          <Divider />
        </Typography>

        <Typography variant="section">
          Play Framework
        </Typography>
        <Typography>
          <ul>
            <li>Session and Flash scopes management with Play: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaSessionFlash">https://www.playframework.com/documentation/2.8.x/JavaSessionFlash</SimpleLink></li>
            <li>Streaming HTTP Responses: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaStream">https://www.playframework.com/documentation/2.8.x/JavaStream</SimpleLink></li>
            <li>Accessing a SQL Database: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/AccessingAnSQLDatabase">https://www.playframework.com/documentation/2.8.x/AccessingAnSQLDatabase</SimpleLink></li>
            <li>Calling REST API-s with Play WS: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaWS">https://www.playframework.com/documentation/2.8.x/JavaWS</SimpleLink></li>
            <li>Internationalization with Messages: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaI18N">https://www.playframework.com/documentation/2.8.x/JavaI18N</SimpleLink></li>
            <li>Security:
              <ul>
                <li>Filters: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/Filters">https://www.playframework.com/documentation/2.8.x/Filters</SimpleLink></li>
                <li>CSP Filters: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/CspFilter">https://www.playframework.com/documentation/2.8.x/CspFilter</SimpleLink></li>
                <li>Allowed Hosts Filter: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/AllowedHostsFilter">https://www.playframework.com/documentation/2.8.x/AllowedHostsFilter</SimpleLink></li>
                <li>Redirect HTTPS FIlter: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/RedirectHttpsFilter">https://www.playframework.com/documentation/2.8.x/RedirectHttpsFilter</SimpleLink></li>
                <li>Cross Site Request Forgery (csrf): <SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaCsrf">https://www.playframework.com/documentation/2.8.x/JavaCsrf</SimpleLink></li>
                <li>Authentication, Authorization (through actions) and Encryption (through serialization and deserialization)</li>
              </ul>
            </li>
          </ul>
        </Typography>

        <Typography variant="section">
          Akka
        </Typography>
        <Typography>
          <ul>
            <li>Cluster Singleton: <SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/cluster-singleton.html">https://doc.akka.io/docs/akka/2.6.0/typed/cluster-singleton.html</SimpleLink></li>
            <li>Distributed Data: <SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/distributed-data.html">https://doc.akka.io/docs/akka/2.6.0/typed/distributed-data.html</SimpleLink></li>
            <li>Akka Streams: <SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/stream/index.html">https://doc.akka.io/docs/akka/2.6.0/stream/index.html</SimpleLink></li>
            <li>and more...</li>
          </ul>
        </Typography>
        <Typography variant="section">
          Mongo DB and Redis
        </Typography>
        <Typography>
          We only kickstarted into it! There is much more to learn. Follow the official documentations or read more at: <SimpleLink href="http://localhost:8080/section/resources/">http://localhost:8080/section/resources/</SimpleLink>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(WhatNext)
