package io.training.api.models;

import io.training.api.models.examples.Membership;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Created by agonlohaj on 29 Sep, 2020
 */
@Data
@RequiredArgsConstructor
public class Person {
	@NonNull
	private String firstName;
	@NonNull
	private String lastName;
	@NonNull
	private Membership membership;

	private Address address = null;
}
