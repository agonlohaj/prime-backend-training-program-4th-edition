package io.training.api.models.requests;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by agonlohaj on 20 Aug, 2020
 */
@Data
public class Node {
	private Integer id = 0;
	private List<Integer> links = new ArrayList<>();
}
