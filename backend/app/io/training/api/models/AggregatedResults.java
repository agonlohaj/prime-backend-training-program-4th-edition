package io.training.api.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AggregatedResults extends BaseModel {
	private String category;
	private String sub;
	private Double total;

	public Double getTotalSquared () {
		return Math.pow(total, 2);
	}
}
